package org.example;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.example.dto.Chart;
import org.example.dto.Column;
import org.example.dto.Type;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GraphController {
    @FXML
    private LineChart<String, Long> lineChart;
    @FXML
    private CategoryAxis xAxis;
    @FXML
    private Button btn;
    //    @FXML
//    private RangeSlider rangeSlider;
    @FXML
    private HBox chartButtonsPane;


    private Chart chart;

    private final GraphDataParser graphDataParser;

    public GraphController() {
        this.graphDataParser = new GraphDataParser();
    }

    private ObservableList<String> xCategories = FXCollections.observableArrayList();

    //    /**
//     * Initializes the controller class. This method is automatically called
//     * after the fxml file has been loaded.
//     */
    @FXML
    private void initialize() {
        xAxis.setCategories(xCategories);

//        // Get an array with the English month names.
//        String[] months = DateFormatSymbols.getInstance(Locale.ENGLISH).getMonths();
//        // Convert it to a list and add it to our ObservableList of months.
//        monthNames.addAll(Arrays.asList(months));
//
//        // Assign the month names as categories for the horizontal axis.
//        xAxis.setCategories(monthNames);
    }

    private void setData(List<Column> columns) {
        Column xColumn = columns.stream()
                .filter(item -> item.getType().equals(Type.X))
                .findFirst()
                .orElseThrow();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-dd");
        for (Long time : xColumn.getValues()) {
            Date date = new Date(time);

            xCategories.add(dateFormat.format(date));
        }
        List<Column> lines = columns.stream()
                .filter(item -> item.getType().equals(Type.LINE))
                .collect(Collectors.toList());
        lines.forEach(
                line -> {
                    XYChart.Series<String, Long> series = new XYChart.Series<>();
                    lineChart.getData().add(series);
                    Button button = new Button();
                    button.setAlignment(Pos.CENTER_LEFT);
                    button.setText(line.getName());
                    button.setStyle("-fx-background-color:" + Utils.toHexColor(line.getColor()));
                    button.setOnMouseClicked(mouseEvent -> {
                        button.setDisable(true);
                        try {
                            if (!lineChart.getData().remove(series)) {
                                lineChart.getData().add(series);
                            }
                        } finally {
                            button.setDisable(false);
                        }
                    });
                    chartButtonsPane.getChildren().add(button);

//                    series.setName(line.getName());
                    List<Long> values = line.getValues();
                    for (int i = 0; i < values.size(); ++i) {
                        series.getData().add(new XYChart.Data<>(xCategories.get(i), values.get(i)));
                    }
                    Node lineNode = series.getNode().lookup(".chart-series-line");
                    lineNode.setStyle("-fx-stroke: "+ Utils.toHexColor(line.getColor()));

//        series.setData();
                }
        );
//        Column line = lines.get(0);
//        // Count the number of people having their birthday in a specific month.
//        XYChart.Series<String, Long> series = new XYChart.Series<>();
//        series.setName(line.getName());
//        List<Long> values = line.getValues();
//        for (int i = 0; i < values.size(); ++i) {
//            series.getData().add(new XYChart.Data<>(xCategories.get(i), values.get(i)));
//        }
//
////        series.setData();
//        lineChart.getData().add(series);
        // Create a XYChart.Data object for each month. Add it to the series.
    }

    @FXML
    private void click(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        Stage dialogStage = new Stage();
        File file = fileChooser.showOpenDialog(dialogStage);
        Chart chart = graphDataParser.parse(file);
        this.setData(chart.getColumns());
    }
}
