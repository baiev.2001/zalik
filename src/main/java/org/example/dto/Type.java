package org.example.dto;

import java.util.Arrays;

public enum Type {
    LINE("line"), X("x");

    private String key;

    Type() {
    }

    Type(String key) {
        this.key = key;
    }

    public static Type of(String value) {
        return Arrays.stream(values())
                .filter(type -> type.key.equals(value))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Type couldn't be found: " + value));
    }
}
