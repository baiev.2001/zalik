package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.example.dto.Chart;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class GraphDataParser {
    private final ObjectMapper objectMapper;
    public GraphDataParser() {
        this.objectMapper = new ObjectMapper();
    }

    public Chart parse(File file) {
        try {
            String json = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            return objectMapper.readValue(json, Chart.class);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

}
