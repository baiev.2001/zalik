package org.example.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import javafx.scene.paint.Color;
import org.example.dto.Chart;
import org.example.dto.Column;
import org.example.dto.Type;

import java.io.IOException;
import java.util.*;

public class ChartDeserializer extends StdDeserializer<Chart> {
    public ChartDeserializer() {
        this(null);
    }

    public ChartDeserializer(Class<Chart> t) {
        super(t);
    }

    @Override
    public Chart deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Chart chartDto = new Chart();
        JsonNode parentNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode chartNode = parentNode.get(0);
        ArrayNode columns = (ArrayNode) chartNode.get("columns");
        Map<String, Column> idColumnDtoMap = new HashMap<>();
        columns.forEach(columnNode -> {
            Iterator<JsonNode> iterator = columnNode.iterator();
            Column columnDto = new Column();
            columnDto.setId(iterator.next().asText());
            List<Long> values = new ArrayList<>();
            iterator.forEachRemaining(jsonNode -> values.add(jsonNode.asLong()));
            columnDto.setValues(values);
            idColumnDtoMap.put(columnDto.getId(), columnDto);
        });
        JsonNode node = chartNode.get("types");
        node.fields().forEachRemaining(jsonNode -> {
            String key = jsonNode.getKey();
            Column column = idColumnDtoMap.get(key);
            column.setType(Type.of(jsonNode.getValue().textValue().trim()));
        });
        JsonNode namesNode = chartNode.get("names");
        namesNode.fields().forEachRemaining(name -> {
            String key = name.getKey();
            Column column = idColumnDtoMap.get(key);
            column.setName(name.getValue().textValue().trim());
        });
        JsonNode colorsNode = chartNode.get("colors");
        colorsNode.fields().forEachRemaining(colorNode -> {
            String key = colorNode.getKey();
            Column column = idColumnDtoMap.get(key);
            column.setColor(Color.valueOf(colorNode.getValue().textValue().trim()));
        });
        chartDto.setColumns(new ArrayList<>(idColumnDtoMap.values()));
        return chartDto;
    }
}
