module org.example {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.controlsfx.controls;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.core;
    requires commons.io;
    requires static lombok;


    exports org.example;
    exports org.example.dto;
    exports org.example.serializer;
    opens org.example to javafx.fxml;
    opens org.example.dto to javafx.fxml;
    opens org.example.serializer to javafx.fxml;
}